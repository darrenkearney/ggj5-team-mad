'use strict';

const Hapi = require('hapi');
const Good = require('good');

const server = new Hapi.Server();
server.connection({ port: 3000 });

server.register(require('inert'), (err) => {

    if (err) {
        throw err;
    }

    server.route([{
                     method: 'GET',
                     path: '/',
                     handler: function (request, reply) {
                         reply.file('./index.html');
                     }
                 },
                {
                    method: 'GET',
                    path: '/bower_components/{path*}',
                    config: {
                        handler: {
                            directory: { path: './bower_components' }
                        },
                        id: 'bower'
                    }
                },
                {
                    method: 'GET',
                    path: '/src/{path*}',
                    config: {
                        handler: {
                            directory: { path: './src' }
                        },
                        id: 'src'
                    }
                },
                {
                    method: 'GET',
                    path: '/assets/{path*}',
                    config: {
                        handler: {
                            directory: { path: './assets' }
                        },
                        id: 'assets'
                    }
                }]
);

});


server.register({
                    register: Good,
                    options: {
                        reporters: [{
                            reporter: require('good-console'),
                            events: {
                                response: '*',
                                log: '*'
                            }
                        }]
                    }
                }, (err) => {

    if (err) {
        throw err; // something bad happened loading the plugin
    }

    server.start((err) => {

    if (err) {
        throw err;
    }
    server.log('info', 'Server running at: ' + server.info.uri);
});
});