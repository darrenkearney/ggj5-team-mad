const SCREEN = { w: 1280, h: 720 }

var Sprites = {

}

var Main = (function() {

    var renderer = PIXI.autoDetectRenderer(SCREEN.w, SCREEN.h,{backgroundColor : 0x000000 });
    document.body.appendChild(renderer.view);

    return {
        renderer : renderer,
        active: null
    }
})();

var MainMenu = (function() {
    var scene = new PIXI.Container();
    var text = new PIXI.Text('Massive Asteroid Destruction',{ fill : 0x00df00 } );
    text.x = (SCREEN.w / 2) - (text.width/2);
    text.y = SCREEN.h / 6;
    scene.addChild(text);

    var items = [
        {
            text: 'Play',
            click: function () {
                Main.active = GameScreen;
            }
        },
        {
            text: 'Credits'
        }
    ];

    var yPos = SCREEN.h / 3;
    items.forEach(function(item) {
        text = new PIXI.Text(item.text, {fill : 0x00df00 });
        text.buttonmode = true;
        text.position.x = (SCREEN.w / 2) - (text.width/2);
        text.position.y = (yPos += (text.height * 1.2));
        text.interactive = true;

        text
            .on('mouseover',
                function() {
                    this.style = {fill: 0xff0000};
                }
        )
            .on('mouseout',
                function() {
                    this.style = {fill: 0x00df00};
                });

        text.click = item.click;

        scene.addChild(text);
    });

    return {
        scene: scene,
        animate: function () {

        }
    }

})();

var GameScreen = (function() {
    var scene = new PIXI.Container();
    var player = {
        sprite: null,
        targetAngle: 0.0
    };

    var loadSprites  = function () {
        var frames = [];
        for (var i = 1; i < 12; i++) {
            var val = i < 10 ? '0' + i : i;

            // magically works since the spritesheet was loaded with the pixi loader
            frames.push(PIXI.Texture.fromFrame('l0_warp-ship' + val + '.png'));
        }

        var sprite = new PIXI.extras.MovieClip(frames);

        sprite.animationSpeed = 0.1;
        sprite.anchor.set( 0.5 );
        sprite.position.x = SCREEN.w  / 2;
        sprite.position.y = SCREEN.h / 2;
        sprite.scale.set( 2 );

        //player.interactive = true;
        sprite.play();

        scene.addChild(sprite);

        player.sprite = sprite;
    };

    var animate = function () {
        if(player) {
            var sprite = player.sprite;
            var mousepos = Main.renderer.plugins.interaction.mouse.global;
            var theta = Math.atan2( mousepos.x - sprite.position.x, sprite.position.y - mousepos.y );
            sprite.rotation = theta;


        }
    };

    return {
        player: player,
        scene: scene,
        onAssetsLoaded: function() {
           loadSprites();
        },
        animate: function () {
            animate();
        }
    }

})();

PIXI.loader
    .add('../assets/images/ship.json')
    .load(onAssetsLoaded);

function onAssetsLoaded() {
    console.log("assets loaded");
    GameScreen.onAssetsLoaded();
    Main.active = MainMenu;

    animate();
}

var animate = (function() {
    requestAnimationFrame(animate);
    Main.active.animate();
    Main.renderer.render( Main.active.scene );
});
